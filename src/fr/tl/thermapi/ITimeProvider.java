package fr.tl.thermapi;

public interface ITimeProvider {

	long currentTimeMillis();

}
