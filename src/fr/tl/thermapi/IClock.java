package fr.tl.thermapi;

/**
 * Horloge hebdomadaire, fournissant les informations jour, heure, minute et
 * seconde.
 * 
 * @author Christophe TOMBELLE
 */
public interface IClock {
	/**
	 * Fournir l'instant courant sous forme du nombre de millisecondes �coul�es
	 * par rapport au d�but de la semaine (anglaise, soit dimanche 00:00:00).
	 * 
	 * @return nombre de millisecondes.
	 */
	int getTime();

	/**
	 * Mettre � l'heure cette horloge.
	 * 
	 * @param iNow
	 *            nombre de millisecondes de l'instant voulu par rapport au
	 *            d�but de la semaine.
	 */
	void setTime(int iNow);

	/**
	 * D�finir le jour de la semaine courant.
	 * 
	 * @param Jour
	 *            de la semaine courant [0-6] avec 0 pour Dimanche.
	 */
	void setDay(int iDay);

	/**
	 * Fournir le jour de la semaine courant.
	 * 
	 * @return Jour de la semaine courant [0-6] avec 0 pour Dimanche.
	 */
	int getDay();

	/**
	 * D�finir l'heure courante.
	 * 
	 * @param Heure
	 *            courante [0-23].
	 */
	void setHour(int iHour);

	/**
	 * Fournir l'heure courante.
	 * 
	 * @return Heure courante [0-23].
	 */
	int getHour();

	/**
	 * D�finir la minute courante [0-59] et remettre � 0 les secondes et
	 * millisecondes.
	 * 
	 * @param Minute
	 *            courante.
	 */
	void setMinute(int iMinute);

	/**
	 * Fournir la minute courante [0-59].
	 * 
	 * @return Minute courante.
	 */
	int getMinute();

	/**
	 * Fournir la seconde courante [0-59].
	 * 
	 * @return Seconde courante.
	 */
	int getSecond();
}
