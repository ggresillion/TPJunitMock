package fr.tl.thermostat;

import fr.tl.thermapi.ITimeProvider;

public class TimeProvider implements ITimeProvider {

	@Override
	public long currentTimeMillis() {
		return System.currentTimeMillis();
	}
	
}
