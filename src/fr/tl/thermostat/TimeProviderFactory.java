package fr.tl.thermostat;

import fr.tl.thermapi.ITimeProvider;

public class TimeProviderFactory {

	protected ITimeProvider timeProvider;

	public TimeProviderFactory(ITimeProvider _timeProvider) {
		timeProvider = _timeProvider;
	}

	public ITimeProvider getTimeProvider() {
		return timeProvider;
	}

	public void setTimeProvider(ITimeProvider timeProvider) {
		this.timeProvider = timeProvider;
	}

}
