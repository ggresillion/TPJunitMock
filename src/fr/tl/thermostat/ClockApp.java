package fr.tl.thermostat;

import java.io.IOException;

public class ClockApp {

	public static void main(String[] args) {
		
		
		TimeProviderFactory factory = new TimeProviderFactory(new TimeProvider());
		Clock clock = new Clock(factory);
		System.out.print(clock.getSecond());
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print(clock.getSecond());

	}

}
