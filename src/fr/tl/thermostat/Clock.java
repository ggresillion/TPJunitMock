/*
 * Clock.java
 *
 * Created on 3 juin 2009, 17:09
 */

package fr.tl.thermostat;

import fr.tl.thermapi.IClock;

/**
 * 
 */
public class Clock implements IClock {
	protected static final int SECOND = 1000;
	protected static final int MINUTE = 60 * SECOND;
	protected static final int HOUR = 60 * MINUTE;
	protected static final int DAY = 24 * HOUR;
	protected static final int WEEK = 7 * DAY;
	protected long _lT0 = 0;
	protected TimeProviderFactory factory;

	public long now() {
		return factory.getTimeProvider().currentTimeMillis();
	}

	/**
	 * Creates a new instance of Clock
	 */
	public Clock(TimeProviderFactory _factory) {
		factory = _factory;
		_lT0 = now();
	}

	public int getDay() {
		return getTime() / DAY;
	}

	public int getHour() {
		return (getTime() / HOUR) % 24;
	}

	public int getMinute() {
		return (getTime() / MINUTE) % 60;
	}

	public int getSecond() {
		return (getTime() / SECOND) % 60;
	}

	public int getTime() {
		return (int) (now() - _lT0) % WEEK;
	}

	public void setDay(int iDay) {
		int currentDay = getDay();
		int ecart = 0;
		if (currentDay < iDay) {
			ecart = iDay - currentDay;
		} else {
			ecart = currentDay - iDay;
		}
		_lT0 = (long) (_lT0 - ecart * DAY);
	}

	public void setHour(int iHour) {
		int actualhour = getHour();
		int ecart = 0;
		if (actualhour < iHour) {
			ecart = iHour - actualhour;
		} else {
			ecart = actualhour - iHour;
		}
		_lT0 = (long) (_lT0 - ecart * HOUR);
	}

	public void setMinute(int iMinute) {
		int actualminute = getMinute();
		int ecart = 0;
		if (actualminute < iMinute) {
			ecart = iMinute - actualminute;
		} else {
			ecart = actualminute - iMinute;
		}
		_lT0 = (long) (_lT0 - ecart * MINUTE);
		// remettre la seconde � 0
		int sec = getSecond();
		_lT0 = (long) ((_lT0 + sec * SECOND));
		int ecart2 = (int) (getTime() % SECOND);
		_lT0 = (long) (_lT0 + ecart2);
	}

	public void setTime(int iNow) {
		_lT0 = (now() - iNow);
	}

}
