package fr.tl.thermostat;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;

import fr.tl.thermapi.ITimeProvider;

public class TestClock {
	
	long value = 21;
	Clock clock;

	@Before
	public void before(){
		ITimeProvider timeProvider = mock(ITimeProvider.class);
		when(timeProvider.currentTimeMillis()).thenReturn(value);
		TimeProviderFactory factory = new TimeProviderFactory(timeProvider);
		clock = new Clock(factory);
	}
	
	@Test
	public void testNow() {
		System.out.println(clock.now());
	}

	@Test
	public void testGetDay() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetHour() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMinute() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetSecond() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTime() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetDay() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetHour() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetMinute() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetTime() {
		fail("Not yet implemented");
	}

}
